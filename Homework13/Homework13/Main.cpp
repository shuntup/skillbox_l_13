#include <iostream>
#include <string>
#include <math.h>

class Vector
{
public:
	Vector() : x(0), y(0), z(0)
	{ }
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{ }

	void Length()
	{
		double v = 0;
		v = sqrt(x * x + y * y + z * z);
		std::cout << "|V|=" << v << "\n";
	}
private: 
	double x;
	double y;
	double z; 
		
};


int main()
{	
	double vX = 0;
	double vY = 0;
	double vZ = 0;

	std::cout << "Enter X: ";
	std::cin >> vX;

	std::cout << "\n" << "Enter Y: ";
	std::cin >> vY;

	std::cout << "\n" << "Enter Z: ";
	std::cin >> vZ;

	std::cout << "\n";

	Vector V(vX, vY, vZ);
	V.Length();

	return 0;
}

